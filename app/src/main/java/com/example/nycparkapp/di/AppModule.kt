package com.example.nycparkapp.di

import android.app.Application
import com.example.nycparkapp.domain.ParkRepository
import com.example.nycparkapp.presentation.parklist.ParkListViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    // todo might need one for the repo

    @Provides
    fun provideParkListViewModel(
        application: Application,
        repo: ParkRepository
        // Other dependencies ...
    ): ParkListViewModel {
        return ParkListViewModel(
            application = application,
            repo = repo
            // Other constructor parameters ...
        )
    }

}