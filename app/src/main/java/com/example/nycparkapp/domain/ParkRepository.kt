package com.example.nycparkapp.domain

import android.util.Log
import com.example.database.domain.ParkEntity
import com.example.database.domain.ParksDao
import com.example.network.data.ParkService
import com.example.network.domain.Multipolygon
import com.example.network.domain.ParksResponseDTO
import com.example.network.domain.ParksResponseDTOItem
import javax.inject.Inject
import javax.sql.StatementEvent

class ParkRepository @Inject constructor(
    private val parkService: ParkService,
    private val parksDao: ParksDao
) {

    suspend fun getParks(): List<ParkEntity> {
        val listDto = parkService.getAllParks()
        return listDto.mapIndexed { index, parksResponseDTOItem ->
            parksResponseDTOItem.toParkItem(index)
        }
    }

    fun Multipolygon.toDBMulti(): Map<Double, Double> {
        val coordinatesMap = mapOf(
            pair = Pair(
                coordinates[0][0][0][0],
                coordinates[0][0][0][1]
            )
        )
        return coordinatesMap
    }


    suspend fun ParksResponseDTOItem.toParkItem(index: Int): ParkEntity {
        val PE = ParkEntity(
            parkId = index + 1,
            acres ?: "",
            borough = mapBoroughCodeToName(borough),
            communityboard ?: "",
            councildistrict ?: "",
            department ?: "",
            description ?: "",
            gispropnum ?: "",
            location ?: "",
//            multipolygon = this.multipolygon.toDBMulti(),
            multipolygon = this.multipolygon.toDBMulti(),
            propname ?: "",
            retired ?: false,
            sitename ?: "",
            subcategory ?: "",
            zipcode ?: ""
        )
        parksDao.insert(PE)
        return PE
    }
}

fun mapBoroughCodeToName(code: String): String {
    return when (code) {
        "X" -> "Bronx"
        "Q" -> "Queens"
        "B" -> "Brooklyn"
        "M" -> "Manhattan"
        "R" -> "Staten Island"
        else -> "Unknown"
    }
}
