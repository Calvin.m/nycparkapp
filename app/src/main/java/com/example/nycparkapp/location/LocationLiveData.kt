package com.example.nycparkapp.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class LocationLiveData(var context: Context) : LiveData<LocationDetails>() {

    private val fusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    override fun onActive() {
        super.onActive()

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e("LocationLiveData", "Permission Check Failed")
            return
        }
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            location.also {
                setLocationData(it)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    internal fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e("LocationLiveData: ", "startLocationUpdates FAILED")
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }


    private fun setLocationData(location: Location?) {
        location?.let { loc ->
            value = LocationDetails(
                longitude = loc.longitude.toString(),
                latitude = loc.latitude.toString()
            )
        }
    }

    override fun onInactive() {
        super.onInactive()
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locResult: LocationResult) {
            super.onLocationResult(locResult)
            Log.e("NEW LOCATION:", "$locResult")
            locResult ?: return // if no result, just return
            for (location in locResult.locations) {
                setLocationData(location = location)
            }
        }
    }

    companion object {
        val ONE_MINUTE: Long = 1000

        @RequiresApi(Build.VERSION_CODES.S)
        val locationRequest: LocationRequest = LocationRequest.create().apply {
            interval = ONE_MINUTE
            fastestInterval = ONE_MINUTE / 4
            priority = android.location.LocationRequest.QUALITY_HIGH_ACCURACY
        }
    }
}

data class LocationDetails(
    val latitude: String,
    val longitude: String
)