package com.example.nycparkapp.navigation

sealed class Screens(val route: String) {
    object ParkListScreen : Screens("parklist")
    object ParkDetailScreen : Screens("detail/{parkId}") {
        fun passArguments(parkId: Int): String {
            return "detail/$parkId"
        }
    }
}