package com.example.nycparkapp.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.nycparkapp.presentation.parklist.ParkListState

@Composable
fun SetUpNavGraph(
    navController: NavHostController,
    parkListState: ParkListState,
//    updateListByBorough: (String) -> Unit
) {
    NavHost(navController = navController, startDestination = Screens.ParkListScreen.route) {
        composable(Screens.ParkListScreen.route) {
            ParkListScreen(
                parkListState,
                parkListState.parkList,
//                updateListByBorough
            )
        }
    }
}
