package com.example.nycparkapp.presentation

import android.Manifest
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.example.nycparkapp.navigation.SetUpNavGraph
import com.example.nycparkapp.presentation.parklist.ParkListViewModel
import com.example.nycparkapp.ui.theme.NYCParkAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val vm by viewModels<ParkListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val updateListByBorough = vm.filterByBorough

        setContent {
            val requestPermissionsLauncher =
                rememberLauncherForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                    if (permissions[Manifest.permission.ACCESS_FINE_LOCATION] == true) {
                        // Permission granted, proceed with fetching location
                        vm.observeUserLocation()
                    } else {
                        // Permission denied, show a message or handle the denial
                        Toast.makeText(
                            this,
                            "Location permission is required to fetch nearby parks.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

//            requestLocationPermissions(requestPermissionsLauncher) // Add this line

            NYCParkAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    val state = vm.state.collectAsState()
                    SetUpNavGraph(
                        navController,
                        state.value,
//                        updateListByBorough = vm.filterByBorough
                    )
                    LaunchedEffect(Unit) { // Add this block
                        requestLocationPermissions(requestPermissionsLauncher)
                    }
                }
            }
        }

    }

    private fun requestLocationPermissions(requestPermissionsLauncher: androidx.activity.result.ActivityResultLauncher<Array<String>>) {
        requestPermissionsLauncher.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }
}
