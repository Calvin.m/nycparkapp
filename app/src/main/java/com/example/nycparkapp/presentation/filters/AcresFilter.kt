package com.example.nycparkapp.presentation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.RangeSlider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import kotlin.math.roundToLong

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AcresFilter(
    modifier: Modifier
) {
    var sliderValues by remember {
        mutableStateOf(0f..100f)
    }

    Column(
        modifier = Modifier.padding(12.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            fontWeight = FontWeight.SemiBold,
            text = "${sliderValues.start.roundToLong()} Acres                ${sliderValues.endInclusive.roundToLong()} Acres"
        )
        Box(modifier = Modifier) {
            RangeSlider(
                values = sliderValues,
                onValueChange = {
                    sliderValues = it
                },
                valueRange = (0f..100f),
                steps = 10
            )
        }
    }
}