package com.example.nycparkapp.presentation

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.DropdownMenu
import androidx.compose.material.Text
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun BoroughFilter(
    modifier: Modifier,
    updateListByBorough: (String) -> Unit
) {
//    var expanded = false
    var expanded by remember { mutableStateOf(false) }
    var selectedBorough by remember { mutableStateOf("All Boroughs") }

    Column(modifier = Modifier) {
        Box(
            modifier = Modifier
                .height(50.dp)
                .padding(8.dp)
        ) {
            Button(
                onClick = { expanded = !expanded }
            ) {
                Text(text = "Borough")
            }
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = {
                    expanded = false
                }) {
                boroughList.forEach { bor ->
                    DropdownMenuItem(
                        text = { Text(text = bor) },
                        onClick = {
                            expanded = false
                            selectedBorough = bor
                            updateListByBorough(bor)
                        }
                    )
                }
            }
        }
        Box(modifier = Modifier.padding(12.dp)) {
            Text(text = selectedBorough, fontWeight = FontWeight.SemiBold)
        }
    }
}