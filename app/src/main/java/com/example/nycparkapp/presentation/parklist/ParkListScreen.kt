package com.example.nycparkapp.navigation

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.database.domain.ParkEntity
import com.example.nycparkapp.presentation.AcresFilter
import com.example.nycparkapp.presentation.BoroughFilter
import com.example.nycparkapp.presentation.parklist.ParkListState
import com.example.nycparkapp.presentation.VerticalListItemSmall
import com.example.nycparkapp.presentation.parklist.ParkListViewModel
import kotlinx.coroutines.flow.observeOn
import kotlinx.coroutines.flow.stateIn

@Composable
fun ParkListScreen(
    parkListState: ParkListState,
    parkList: List<ParkEntity>,
    viewModel: ParkListViewModel = hiltViewModel(),
) {
    val pListState = viewModel.state.collectAsState().value.parkList

    Column {
        Row(modifier = Modifier.background(Color(192, 179, 230, 255))) {
            BoroughFilter(
                modifier = Modifier.weight(1f),
                updateListByBorough = { borough ->
                    viewModel.filterByBorough(borough)
                }
            )
            AcresFilter(modifier = Modifier.weight(1f))
        }
        if (parkListState.isLoading) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator()
            }
        } else {
            ParkList(list = pListState)
        }
    }
}

@Composable
fun ParkList(list: List<ParkEntity>) {
    LazyColumn {
        items(list) { park ->
            VerticalListItemSmall(park)
            Divider(
                modifier = Modifier.padding(horizontal = 12.dp, vertical = 12.dp),
                color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.08f)
            )
        }
    }
}