package com.example.nycparkapp.presentation.parklist

import com.example.database.domain.ParkEntity

data class ParkListState(
    val isLoading: Boolean = true,
    val parkList: List<ParkEntity> = emptyList()
)
