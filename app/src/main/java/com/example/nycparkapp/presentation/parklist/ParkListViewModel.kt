package com.example.nycparkapp.presentation.parklist

import android.app.Application
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.room.Dao
import com.example.database.domain.ParkEntity
import com.example.database.domain.ParksDao
import com.example.nycparkapp.domain.ParkRepository
import com.example.nycparkapp.location.LocationLiveData
import com.example.nycparkapp.presentation.boroughList
import com.example.nycparkapp.presentation.sortParksByDistance
import com.example.nycparkapp.presentation.util.Constants.BRONX
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ParkListViewModel @Inject constructor(
    private val repo: ParkRepository,
    private val application: Application
) : ViewModel() {

    private val _state = MutableStateFlow(ParkListState())
    val state: StateFlow<ParkListState> get() = _state

    private val locationLiveData: LocationLiveData

    init {
        locationLiveData = LocationLiveData(application)
        observeUserLocation()
    }

    fun observeUserLocation() {
        locationLiveData.observeForever { locationDetails ->
            val latitude = locationDetails.latitude.toDouble()
            val longitude = locationDetails.longitude.toDouble()
            viewModelScope.launch {
                fetchParkList(latitude, longitude)
            }
        }
    }


    fun fetchParkList(lat: Double, lon: Double) {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)!!
            val parkList = repo.getParks()
            val orderedParkList = parkList.sortParksByDistance(lat, lon)
            _state.value = _state.value?.copy(isLoading = false, parkList = orderedParkList)!!
        }
    }

    fun filterByBorough(string: String) {
        viewModelScope.launch {
            _state.value = _state.value.copy(isLoading = true)
            val newList = repo.getParks().filter { it.borough == string }
            _state.value = _state.value.copy(isLoading = false, parkList = newList)
        }
    }

    override fun onCleared() {
        super.onCleared()
//        locationLiveData.removeObserver()
    }
}
