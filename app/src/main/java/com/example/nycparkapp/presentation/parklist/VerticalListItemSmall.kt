package com.example.nycparkapp.presentation

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.IconToggleButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.database.domain.ParkEntity

//import com.guru.composecookbook.data.DemoDataProvider
//import com.guru.composecookbook.data.model.Item
//import com.guru.composecookbook.theme.ComposeCookBookTheme

@Composable
fun VerticalListItemSmall(item: ParkEntity, modifier: Modifier = Modifier) {
    val typography = MaterialTheme.typography
    Row(
        modifier = Modifier
            .clickable(onClick = {
                Log.e("Park Clicked:", "$item")
            })
            .padding(16.dp)
    ) {
//        ItemImage(
//            item,
//            Modifier.padding(end = 16.dp)
//        )
        Column(modifier = Modifier.weight(1f)) {
            Text(item.sitename, style = typography.titleMedium)
//            Text(item.propname, style = typography.bodyMedium)
//            Text(item.location, style = typography.bodyMedium)
            Text(item.borough, style = typography.bodyMedium)
            Text(item.acres, style = typography.bodyMedium)

        }
        FavIcon(modifier)
    }
}

//@Composable
//fun ItemImage(item: Item, modifier: Modifier = Modifier) {
//    Image(
//        painter = painterResource(id = item.imageId),
//        contentScale = ContentScale.Crop,
//        contentDescription = null,
//        modifier = modifier
//            .size(100.dp, 80.dp)
//            .clip(androidx.compose.material.MaterialTheme.shapes.medium)
//    )
//}

@Composable
fun FavIcon(modifier: Modifier = Modifier) {
    val isFavourite = remember { mutableStateOf(false) }
    IconToggleButton(
        checked = isFavourite.value,
        onCheckedChange = { isFavourite.value = !isFavourite.value }
    ) {
        if (isFavourite.value) {
            Icon(
                imageVector = Icons.Filled.Favorite,
                contentDescription = null,
                modifier = modifier,
                tint = Color(255, 0, 0, 126)
            )
        } else {
            Icon(
                imageVector = Icons.Default.FavoriteBorder,
                contentDescription = null,
                modifier = modifier
            )
        }
    }
}