package com.example.nycparkapp.presentation.util

object Constants {
    const val BRONX = "Bronx"
    const val QUEENS = "Queens"
    const val BROOKLYN = "Brooklyn"
    const val MANHATTAN = "Manhattan"
    const val STATEN_ISLAND = "Staten Island"

    val BoroughList = listOf(BRONX, QUEENS, BROOKLYN, MANHATTAN, STATEN_ISLAND)
}