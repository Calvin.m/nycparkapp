package com.example.nycparkapp.presentation

import android.util.Log
import com.example.database.domain.ParkEntity

fun haversineDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
    val earthRadius = 6371 // Earth's radius in kilometers

    val dLat = Math.toRadians(lat2 - lat1)
    val dLon = Math.toRadians(lon2 - lon1)

    val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)

    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

    return earthRadius * c
}

fun List<ParkEntity>.sortParksByDistance(userLat: Double, userLon: Double): List<ParkEntity> {
    return this.sortedBy { park ->

//        val lat = park.multipolygon.coordinates[0][0][0][0]
//        val lon = park.multipolygon.coordinates[0][0][0][1]

//        Log.e("LAT-value: ", "${park.multipolygon.entries.first().value}")
//        Log.e("LON-key: ", "${park.multipolygon.entries.first().key}")

        haversineDistance(
            userLat,
            userLon,
            park.multipolygon.entries.first().value,
            park.multipolygon.entries.first().key
        )
    }
}

val boroughList: List<String> =
    listOf("All Boroughs", "Bronx", "Queens", "Brooklyn", "Manhattan", "Staten Island")
