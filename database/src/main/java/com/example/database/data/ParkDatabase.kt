package com.example.database.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.database.domain.Converters
import com.example.database.domain.ParkEntity
import com.example.database.domain.ParksDao


@Database(
    entities = [ParkEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class ParkDatabase : RoomDatabase() {

    abstract fun parksDao(): ParksDao

    companion object {
        @Volatile
        private var INSTANCE: ParkDatabase? = null

        fun getInstance(context: Context): ParkDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ParkDatabase::class.java,
                    "parks-database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }

}