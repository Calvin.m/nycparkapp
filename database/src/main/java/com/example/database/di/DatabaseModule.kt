package com.example.database.di

import android.content.Context
import com.example.database.data.ParkDatabase
import com.example.database.domain.ParksDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    fun provideParkDao(parkDatabase: ParkDatabase) : ParksDao {
        return parkDatabase.parksDao()
    }

    @Provides
    @Singleton
    fun provideParkDatabase(@ApplicationContext context: Context): ParkDatabase {
        return ParkDatabase.getInstance(context)
    }

}