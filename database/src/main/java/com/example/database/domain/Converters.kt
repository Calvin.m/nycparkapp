package com.example.database.domain

import androidx.room.TypeConverter
import com.example.database.domain.Multipolygon
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    @TypeConverter
    fun fromMultipolygon(multipolygon: Multipolygon): String {
        return Gson().toJson(multipolygon)
    }

    @TypeConverter
    fun toMultipolygon(json: String): Multipolygon {
        val type = object : TypeToken<Multipolygon>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun fromMap(map: Map<Double, Double>): String {
        return Gson().toJson(map)
    }

    @TypeConverter
    fun toMap(json: String): Map<Double, Double> {
        val type = object : TypeToken<Map<Double, Double>>() {}.type
        return Gson().fromJson(json, type)
    }
}