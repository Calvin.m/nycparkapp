package com.example.database.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity("parks")
data class ParkEntity(
    @PrimaryKey(autoGenerate = true) val parkId: Int?,
    val acres: String,
    val borough: String,
    val communityboard: String,
    val councildistrict: String,
    val department: String,
    val description: String,
    val gispropnum: String,
    val location: String,
//    val multipolygon: Multipolygon,
    val multipolygon: Map<Double, Double>,
    val propname: String,
    val retired: Boolean,
    val sitename: String,
    val subcategory: String,
    val zipcode: String
)
