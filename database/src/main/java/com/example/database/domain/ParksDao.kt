package com.example.database.domain

import androidx.room.*

@Dao
interface ParksDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(park: ParkEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(parkList: List<ParkEntity>)

    @Query("SELECT * FROM parks")
    suspend fun getParks(): List<ParkEntity>
}
