package com.example.network.data

import com.example.network.domain.ParksResponseDTO
import com.example.network.domain.ParksResponseDTOItem
import retrofit2.http.GET

interface ParkService {
    @GET("4j29-i5ry.json/")
    suspend fun getAllParks(): List<ParksResponseDTOItem>
}

object Util {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
}