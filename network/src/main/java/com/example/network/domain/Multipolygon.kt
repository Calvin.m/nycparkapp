package com.example.network.domain

data class Multipolygon(
    val coordinates: List<List<List<List<Double>>>>,
    val type: String
)