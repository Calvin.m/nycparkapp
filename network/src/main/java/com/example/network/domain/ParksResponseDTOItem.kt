package com.example.network.domain

data class ParksResponseDTOItem(
    val acres: String,
    val borough: String,
    val communityboard: String,
    val councildistrict: String,
    val department: String,
    val description: String,
    val gispropnum: String,
    val location: String,
    val multipolygon: Multipolygon,
    val propname: String,
    val retired: Boolean,
    val sitename: String,
    val subcategory: String,
    val zipcode: String
)